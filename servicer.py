import sys
import io
import torch
import librosa
import numpy as np
from scipy.io.wavfile import read

from model.model import VoiceFilter
from model.makewav import WavMaker as WM
from utils.hparams import load_hparam_str
from utils.audio import Audio

from dvectorize_client import DvectorizeClient
from dap_pb2_grpc import VfilterServicer
from dap_pb2 import EmbConfig, WavBinary

chunk_size=3145728
max_t = 1024

def load_checkpoint(chkpt_path):
    checkpoint = torch.load(chkpt_path, map_location='cpu')
    model = checkpoint['model']
    hp_str = checkpoint['hp_str']
    hp = load_hparam_str(hp_str)

    return hp, model

def WavToBytes(wavmaker, wavdata):
   wavlist = wavdata.cpu().detach().numpy()
   wavint = np.int16(wavlist / (np.max(np.absolute(wavlist)) + 0.001) * 32767)
   wav_bytes = wavmaker.make_header(len(wavint))
   wav_bytes += wavmaker.make_data(wavint)
   return wav_bytes


def wav2spec(audio, y):
    D = audio.stft(y)
    C = torch.view_as_complex(D)
    S, P = C.abs(), C.angle()
    S, P = torch.transpose(S, 0, 1), torch.transpose(P, 0, 1)  # to make [time, freq]
    return D.transpose(0, 1), S, P

class VoicefilterInference(VfilterServicer):
    def __init__(self, args):
        hp, state_dict = load_checkpoint(args.model)
        self.hp = hp
        self.audio = Audio(hp)

        torch.cuda.set_device(args.device)
        self.device = args.device
        self.model = VoiceFilter(hp).cuda()
        self.model.load_state_dict(state_dict)
        self.model.eval()

        self.emb_config = EmbConfig(
            emb_dim=hp.embedder.emb_dim
        )

        self.emb_dim = self.emb_config.emb_dim

        self.dvec_client = DvectorizeClient(
            remote=args.dvectorize_remote)

        self.wavmaker = WM(self.hp.audio.sample_rate)

        del hp
        del state_dict
        torch.cuda.empty_cache()

    def SeparateTarget(self, mixedandref, context):
        dveclist = list()
        wav = bytearray()
        for mixedandref_iterator in mixedandref:
            if mixedandref_iterator.ref != b'':
                emb = self.dvec_client.get_dvec_from_wav(mixedandref_iterator.ref).data
                assert len(emb) % self.emb_dim == 0
                emb = torch.Tensor(emb)
                emb = emb.view(-1, self.emb_dim)
                dveclist.append(emb)

            wav.extend(mixedandref_iterator.mixed)

        dvec = torch.cat(dveclist, dim=0)
        dvec = torch.sum(dvec, dim=0) / dvec.shape[0]

        torch.cuda.set_device(self.device)
        with torch.no_grad():
            wav = io.BytesIO(wav)
            sampling_rate, wav = read(wav)

            if len(wav.shape) == 2:
                wav = wav[:, 0]

            if wav.dtype == np.int16:
                wav = wav / 32768.0
            elif wav.dtype == np.int32:
                wav = wav / 2147483648.0
            elif wav.dtype == np.uint8:
                wav = (wav - 128) / 128.0

            wav = wav.astype(np.float32)

            if sampling_rate != self.hp.audio.sample_rate:
                wav = librosa.resample(wav, sampling_rate,
                                       self.hp.audio.sample_rate)
                wav = np.clip(wav, -1.0, 1.0)

            wav = torch.from_numpy(wav)
            D, mixed_mag, mixed_phase = wav2spec(self.audio, wav)
            D = D.unsqueeze(0).cuda()
            mixed_mag = mixed_mag.unsqueeze(0).cuda()
            mixed_phase = mixed_phase.unsqueeze(0).cuda()
            dvec = dvec.unsqueeze(0).cuda()

            est_mag_list = []
            est_phase_list = []
            time_length = mixed_mag.shape[1]
            for i in range(time_length//max_t + (time_length % max_t != 0)):
                mixed_mag_chunk = mixed_mag[:, max_t * i:min(max_t * (i+1), time_length),:]
                mixed_phase_chunk = mixed_phase[:, max_t * i:min(max_t * (i+1), time_length),:]
                mixedspec = D[:, max_t * i:min(max_t * (i+1), time_length), :, :]

                mask_mag, mask_phase = self.model(mixedspec, dvec)
                mask_mag, mask_phase = mask_mag.detach(), mask_phase.detach()

                est_mag = mixed_mag_chunk * mask_mag

                est_phase = torch.stack((torch.cos(mixed_phase_chunk) * mask_phase[..., 0] - torch.sin(mixed_phase_chunk) * mask_phase[..., 1], \
                                         torch.sin(mixed_phase_chunk) * mask_phase[..., 0] + torch.cos(mixed_phase_chunk) * mask_phase[..., 1]), dim=-1)
                est_mag_list.append(est_mag)
                est_phase_list.append(est_phase)

                del mixed_mag_chunk
                del mixed_phase_chunk
                del mixedspec
                del mask_mag
                del mask_phase
                del est_mag
                del est_phase

            est_mag = torch.cat(est_mag_list, dim=1)
            est_phase = torch.cat(est_phase_list, dim=1)

            est_wav = self.audio.spec2wav(est_mag[0], est_phase[0])

            est_wav_bytes = WavToBytes(self.wavmaker, est_wav)
            for i in range((len(est_wav_bytes) - 1) // chunk_size + 1):
                yield WavBinary(bin=est_wav_bytes[chunk_size * i: chunk_size * (i + 1)])
            del dvec
            del dveclist
            del wav
            del D
            del mixed_mag
            del mixed_phase
            del est_mag
            del est_mag_list
            del est_phase
            del est_phase_list
            del est_wav
            del est_wav_bytes
            torch.cuda.empty_cache()

    def GetEmbConfig(self, empty, context):
        return self.emb_config
