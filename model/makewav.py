import numpy as np
import sys
import struct

class WavMaker:
    def __init__(self, sample_rate = None, channels=1, dtype=np.dtype(np.int16)):
        self.dtype = dtype
        self.channels = channels
        self.sample_rate = sample_rate if sample_rate is not None else 16000
        self.WAVE_FORMAT_PCM = 0x0001
        self.WAVE_FORMAT_IEEE_FLOAT = 0x0003

    def make_header(self, sample_length):
        nbytes = sample_length * self.channels * self.dtype.itemsize
        dkind = self.dtype.kind
        if not (dkind == 'i' or dkind == 'f' or (dkind == 'u' and
                                                 self.dtype.itemsize == 1)):
            raise ValueError("Unsupported data type '%s'" % self.dtype)

        header_data = b''

        header_data += b'RIFF'
        header_data += struct.pack('<I', nbytes + 36)
        header_data += b'WAVE'

        # fmt chunk
        header_data += b'fmt '
        if dkind == 'f':
            format_tag = self.WAVE_FORMAT_IEEE_FLOAT
        else:
            format_tag = self.WAVE_FORMAT_PCM
        bit_depth = self.dtype.itemsize * 8
        bytes_per_second = self.sample_rate*(bit_depth // 8)*self.channels
        block_align = self.channels * (bit_depth // 8)

        fmt_chunk_data = struct.pack('<HHIIHH', format_tag, self.channels, self.sample_rate,
                                     bytes_per_second, block_align, bit_depth)
        if not (dkind == 'i' or dkind == 'u'):
            # add cbSize field for non-PCM files
            fmt_chunk_data += b'\x00\x00'

        header_data += struct.pack('<I', len(fmt_chunk_data))
        header_data += fmt_chunk_data

        # fact chunk (non-PCM files)
        if not (dkind == 'i' or dkind == 'u'):
            header_data += b'fact'
            header_data += struct.pack('<II', 4, sample_length)

        # check data size (needs to be immediately before the data chunk)
        if ((len(header_data)-4-4) + (4+4+nbytes)) > 0xFFFFFFFF:
            raise ValueError("Data exceeds wave file size limit")

        header_data += b'data'
        header_data += struct.pack('<I', nbytes)
        return header_data

    def make_data(self, data):
        if self.dtype.byteorder == '>' or (self.dtype.byteorder == '=' and
                                           sys.byteorder == 'big'):
            data = data.byteswap()
        return data.ravel().view('b').data
