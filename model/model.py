import torch
import torch.nn as nn
import torch.nn.functional as F

class CompConv2d(nn.Module):
    #in_c and out_c for r/i channel not r+i channel
    def __init__(self, in_c, out_c, filt, str, pad, act):
        super(CompConv2d, self).__init__()
        self.padding = nn.ZeroPad2d(pad)
        self.conv_r = nn.Conv2d(in_c, out_c, kernel_size=filt, stride=str)
        self.conv_i = nn.Conv2d(in_c, out_c, kernel_size=filt, stride=str)
        self.bn_r = nn.BatchNorm2d(out_c)
        self.bn_i = nn.BatchNorm2d(out_c)
        self.act = act

    def forward(self, x):
        #x:[B,C,T,F,2(r,i)]
        r,i =x[...,0], x[...,1]
        r = self.padding(r)
        i = self.padding(i)
        real = self.conv_r(r)- self.conv_i(i)
        imag = self.conv_r(i)+ self.conv_i(r)
        real = self.bn_r(real)
        imag = self.bn_i(imag)
        o = torch.stack((real,imag), dim =-1)
        o = self.act(o)
        return o

class CompdeConv2d(nn.Module):
    #in_c and out_c for r/i channel not r+i channel
    def __init__(self, in_c, out_c, filt, str, pad, act, out_pad=0):
        super(CompdeConv2d, self).__init__()
        self.padding = nn.ZeroPad2d(pad)
        self.deconv_r = nn.ConvTranspose2d(in_c, out_c, kernel_size=filt, stride=str, padding=(1, 0), output_padding=out_pad)
        self.deconv_i = nn.ConvTranspose2d(in_c, out_c, kernel_size=filt, stride=str, padding=(1, 0), output_padding=out_pad)
        self.bn_r = nn.BatchNorm2d(out_c)
        self.bn_i = nn.BatchNorm2d(out_c)
        self.act = act

    def forward(self, x, x1):
        #x:[B,C,T,F,2(r,i)]
        x = torch.cat((x, x1), dim=1)
        r,i =x[...,0], x[...,1]
        r = self.padding(r)
        i = self.padding(i)
        real = self.deconv_r(r)- self.deconv_i(i)
        imag = self.deconv_r(i)+ self.deconv_i(r)
        real = self.bn_r(real)
        imag = self.bn_i(imag)
        o = torch.stack((real,imag), dim =-1)
        o = self.act(o)
        return o

class CompLSTM(nn.Module):
    #in_c and out_c for r/i channel not r+i channel
    def __init__(self, in_c, out_c, b_first, bidirec=False):
        super(CompLSTM, self).__init__()
        self.LSTM_r = self.lstm = nn.LSTM(in_c, out_c, batch_first=b_first, bidirectional=bidirec)
        self.LSTM_i = self.lstm = nn.LSTM(in_c, out_c, batch_first=b_first, bidirectional=bidirec)

    def forward(self, x):
        #x:[B,T,F,2(r,i)]
        r,i =x[...,0], x[...,1]
        lstm_rr, _ = self.LSTM_r(r)
        lstm_ii, _ = self.LSTM_i(i)
        lstm_ri, _ = self.LSTM_r(i)
        lstm_ir, _ = self.LSTM_i(r)
        real = lstm_rr - lstm_ii
        imag = lstm_ri + lstm_ir
        o = torch.stack((real,imag), dim =-1)
        return o

class CompLinear(nn.Module):
    #in_c and out_c for r/i channel not r+i channel
    def __init__(self, in_c, out_c, act):
        super(CompLinear, self).__init__()
        self.fc_r = nn.Linear(in_c, out_c)
        self.fc_i = nn.Linear(in_c, out_c)
        self.act = act

    def forward(self, x):
        #x:[B,C,F,T,2(r,i)]
        r,i =x[...,0], x[...,1]
        real = self.fc_r(r)- self.fc_i(i)
        imag = self.fc_r(i)+ self.fc_i(r)
        o = torch.stack((real,imag), dim =-1)
        o = self.act(o)
        return o

class VoiceFilter(nn.Module):
    def __init__(self, hp):
        super(VoiceFilter, self).__init__()
        self.hp = hp
        assert hp.audio.n_fft // 2 + 1 == hp.audio.num_freq, \
            "stft-related dimension mismatch"
        # (1, T, 257)
        # cnn1
        self.conv1=CompConv2d(1, 32, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (32, T, 127)

        # cnn2
        self.conv2=CompConv2d(32, 64, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (64, T, 62)

        # cnn3
        self.conv3=CompConv2d(64, 128, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (128, T, 29)

        # cnn4
        self.conv4=CompConv2d(128, 256, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (256, T, 13)

        # cnn5
        self.conv5=CompConv2d(256, 256, filt=(2, 5), str=(1,2),pad=(0,0,1,0), act=nn.ReLU()) # (256, T, 5)

        # cnn6
        self.conv6=CompConv2d(256, 256, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (256, T, 1)

        self.lstm1 = CompLSTM(512, 1024, b_first=True)
        self.lstm2 = CompLSTM(1024, 1024, b_first=True)
        self.fc = CompLinear(1024, 256, act=nn.ReLU()) 

        # (256, T, 1)
        # decnn6
        self.deconv6=CompdeConv2d(512, 256, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (256, T, 5)

        # decnn5
        self.deconv5=CompdeConv2d(512, 256, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (256, T, 13)

        # decnn4
        self.deconv4=CompdeConv2d(512, 128, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (128, T, 29)

        # decnn3
        self.deconv3=CompdeConv2d(256, 64, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU(), out_pad=(0,1)) # (64, T, 62)

        # decnn2
        self.deconv2=CompdeConv2d(128, 32, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.ReLU()) # (32, T, 127)

        # decnn1
        self.deconv1=CompdeConv2d(64, 1, filt=(2, 5), str=(1,2), pad=(0,0,1,0), act=nn.Tanh()) # (1, T, 257)

        self._initialize_weights()

    def forward(self, x, dvec):
        # x: [B, T, num_freq, 2(r,i)]
        x = x.unsqueeze(1)
        # x: [B, 1, T, 257, 2(r,i)]
        x1 = self.conv1(x)
        # x: [B, 32, T, 127, 2(r,i)]
        x2 = self.conv2(x1)
        # x: [B, 64, T, 62, 2(r,i)]
        x3 = self.conv3(x2)
        # x: [B, 128, T, 29, 2(r,i)]
        x4 = self.conv4(x3)
        # x: [B, 256, T, 13, 2(r,i)]
        x5 = self.conv5(x4)
        # x: [B, 256, T, 5, 2(r,i)]
        x6 = self.conv6(x5)
        # x: [B, 256, T, 1, 2(r,i)]

        x = x6.transpose(1, 2).contiguous()
        # x: [B, T, 256, 1, 2(r,i)]
        x = x.view(x.size(0), x.size(1), -1, 2)
        # x: [B, T, 256*1, 2(r,i)]

        x_r, x_i = x[..., 0], x[..., 1]
        # dvec: [B, emb_dim]
        dvec = dvec.unsqueeze(1)
        # dvec: [B, 1, emb_dim]
        dvec = dvec.repeat(1, x.size(1), 1)
        # dvec: [B, T, emb_dim]
        x_r = torch.cat((x_r, dvec), dim=2)  # [B, T, 256*1 + 256 = 512]
        x_i = torch.cat((x_i, dvec), dim=2)  # [B, T, 512]
        x = torch.stack((x_r, x_i), dim=-1)  # [B, T, 512, 2(r,i)]

        x = self.lstm1(x) # [B, T, 256, 2(r,i)]
        x = F.relu(x)
        x = self.lstm2(x) # [B, T, 256, 2(r,i)]
        x = F.relu(x)
        x = self.fc(x) # [B, T, 256, 2(r,i)]

        x = x.view(x.size(0), x.size(1), 256, 1, 2)
        # x: [B, T, 256, 1, 2(r,i)]
        x = x.transpose(1, 2).contiguous()
        # x: [B, 256, T, 1, 2(r,i)]

        x = self.deconv6(x, x6)
        # x: [B, 256, T, 7, 2(r,i)]
        x = self.deconv5(x, x5)
        # x: [B, 256, T, 15, 2(r,i)]
        x = self.deconv4(x, x4)
        # x: [B, 128, T, 31, 2(r,i)]
        x = self.deconv3(x, x3)
        # x: [B, 64, T, 63, 2(r,i)]
        x = self.deconv2(x, x2)
        # x: [B, 32, T, 128, 2(r,i)]
        x = self.deconv1(x, x1)
        # x: [B, 1, T, 257, 2(r,i)]

        x = x.transpose(1, 2).contiguous()
        # x: [B, T, 1, 257, 2(r,i)]
        x = x.view(x.size(0), x.size(1), -1, 2)
        # x: [B, T, 257, 2]
    
        mask_mag = torch.norm(x, dim=-1) # [B, T, 257] 
        #mask_phase = x / mask_mag.unsqueeze(3).repeat(1, 1, 1, 2)
        mask_mag = 2*torch.tanh(mask_mag)
        mask_phase = F.normalize(x, dim=-1) # [B, T, 257, 2(r,i)]
        return mask_mag, mask_phase

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, (nn.Conv2d, nn.ConvTranspose2d)):
                _fan = nn.init._calculate_correct_fan(m.weight, 'fan_out')
                _bound = (6. /_fan) ** 0.5
                nn.init.uniform_(m.weight,-_bound,_bound)
                if m.bias is not None:
                    nn.init.constant_(m.bias,0)
                    if isinstance(m, (nn.BatchNorm2d, nn.Linear)):
                        nn.init.constant_(m.weight,0.1)
                        nn.init.constant_(m.bias,0)
