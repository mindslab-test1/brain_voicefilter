FROM pytorch/pytorch:1.6.0-cuda10.1-cudnn7-devel

RUN python3 -m pip --no-cache-dir install --upgrade \
	librosa==0.8.0 \
	matplotlib==3.3.3 \
	mir_eval==0.6 \
	tensorboard==2.3.0 \
	gpustat==0.6.0 \
	grpcio==1.13.0 \
	grpcio-tools==1.13.0 \
	protobuf==3.6.0 \
	&& \
apt update && \
apt install -y \
	ffmpeg \
	htop \
	vim \
	tmux && \
mkdir /root/voicefilter
COPY . /root/voicefilter
RUN cd /root/voicefilter/ && \
    python3 -m grpc.tools.protoc --proto_path=brain_idl/protos/audio/legacy --python_out=. --grpc_python_out=. dap.proto
