import os
import glob
import torch
import torch.nn as nn
import torch.nn.functional as F
import librosa
import argparse
import numpy as np
from mir_eval.separation import bss_eval_sources
from tqdm import tqdm

from utils.audio import Audio
from utils.hparams import HParam
from model.model import VoiceFilter
from model.embedder import SpeechEmbedder


def main(args, hp):
    def wav2magphase(path):
        wav, _ = librosa.load(path, hp.audio.sample_rate)
        wav = torch.from_numpy(wav).float()
        mag, phase = audio.wav2spec(wav)
        return mag, phase

    def find_all(file_format):
        return sorted(glob.glob(os.path.join(args.data_dir, file_format)))

    dvec_list_f = find_all(hp.form.dvec)
    target_wav_list_f = find_all(hp.form.target.wav)
    mixed_wav_list_f = find_all(hp.form.mixed.wav)
    assert len(dvec_list_f) == len(target_wav_list_f) == len(mixed_wav_list_f), "number of training files must match"
    length = len(dvec_list_f)

    dvec_list = list()
    target_wav_list = list()
    mixed_wav_list = list()
    target_mag_list = list()
    target_phase_list = list()
    mixed_mag_list = list()
    mixed_phase_list = list()

    audio = Audio(hp)
    for idx in range(length):
        with open(dvec_list_f[idx], 'r') as f:
            dvec_path = f.readline().strip()
        dvec_path = os.path.splitext(dvec_path)[0] + '_vec.pt'
        dvec = torch.load(dvec_path)

        target_wav, _ = librosa.load(target_wav_list_f[idx], hp.audio.sample_rate)
        target_wav = torch.from_numpy(target_wav)
        mixed_wav, _ = librosa.load(mixed_wav_list_f[idx], hp.audio.sample_rate)
        mixed_wav = torch.from_numpy(mixed_wav)
        target_mag, target_phase = wav2magphase(target_wav_list_f[idx])
        mixed_mag, mixed_phase = wav2magphase(mixed_wav_list_f[idx])

        dvec_list.append(dvec)
        target_wav_list.append(target_wav)
        mixed_wav_list.append(mixed_wav)
        target_mag_list.append(target_mag)
        target_phase_list.append(target_phase)
        mixed_mag_list.append(mixed_mag)
        mixed_phase_list.append(mixed_phase)

    dvec = torch.stack(dvec_list, dim=0)
    target_wav = torch.stack(target_wav_list, dim=0)
    mixed_wav = torch.stack(mixed_wav_list, dim=0)
    target_mag = torch.stack(target_mag_list, dim=0)
    target_phase = torch.stack(target_phase_list, dim=0)
    mixed_mag = torch.stack(mixed_mag_list, dim=0)
    mixed_phase = torch.stack(mixed_phase_list, dim=0)

    with torch.no_grad():
        model = VoiceFilter(hp).cuda()
        chkpt_model = torch.load(args.checkpoint_path)['model']
        model.load_state_dict(chkpt_model)
        model.eval()

        audio = Audio(hp)

        dvec = dvec.cuda()
        mixed_mag = mixed_mag.cuda()
        mixed_phase = mixed_phase.cuda()
        mixed = torch.stack((mixed_mag * torch.cos(mixed_phase), mixed_mag * torch.sin(mixed_phase)), dim=-1).cuda()
        
        mask_mag_list = list()
        mask_phase_list = list()
        pbar = tqdm(total=length)
        for i in range(length):
            pbar.update(1)
            mixed1 = mixed[i].unsqueeze(0).cuda()
            dvec1 = dvec[i].unsqueeze(0).cuda()
            mask_mag, mask_phase = model(mixed1, dvec1)
            mask_mag, mask_phase = mask_mag.cpu().detach().squeeze(0), mask_phase.cpu().detach().squeeze(0)
            mask_mag_list.append(mask_mag)
            mask_phase_list.append(mask_phase)
        pbar.close()
        mask_mag = torch.stack(mask_mag_list, dim=0).cuda()
        mask_phase = torch.stack(mask_phase_list, dim=0).cuda()

        est_mag = mask_mag * mixed_mag

        mask_phase_cos, mask_phase_sin = mask_phase[..., 0], mask_phase[..., 1]
        est_phase_cos = torch.cos(mixed_phase) * mask_phase_cos - torch.sin(mixed_phase) * mask_phase_sin
        est_phase_sin = torch.sin(mixed_phase) * mask_phase_cos + torch.cos(mixed_phase) * mask_phase_sin
        est_phase = torch.stack((est_phase_cos, est_phase_sin), dim=-1)

        est_wav_list = list()
        for i in range(length):
            est_mag1 = est_mag[i]
            est_phase1 = est_phase[i]

            est_wav = audio.spec2wav(est_mag1, est_phase1)
            est_wav_list.append(est_wav)
        est_wav = torch.stack(est_wav_list, dim=0)
        est_wav = est_wav.cpu().detach().numpy()
        target_wav = target_wav.numpy()
        mixed_wav = mixed_wav.numpy()
        
        sdr_before_list = list()
        sdr_list = list()
        pbar = tqdm(total=length)
        for i in range(length):
            pbar.update(1)
            sdr_before = bss_eval_sources(target_wav[i], mixed_wav[i], False)[0]
            sdr = bss_eval_sources(target_wav[i], est_wav[i], False)[0]
            sdr_before_list.append(sdr_before)
            sdr_list.append(sdr)
        pbar.close()
        sdr_before = np.stack(sdr_before_list, axis=0)
        sdr = np.stack(sdr_list, axis=0)

        sdr_median_b = np.median(sdr_before)
        sdr_mean_b = np.mean(sdr_before)

        sdr_median = np.median(sdr)
        sdr_mean = np.mean(sdr)
        print(sdr)
        n_count = 0
        for s in sdr:
            if s < 0:
                n_count += 1
        print("num of negative: ", n_count, '/', len(sdr))
        print("sdr_median_before: ", sdr_median_b, "sdr_mean_before: ", sdr_mean_b)
        print("sdr_median: ", sdr_median, "sdr_mean: ", sdr_mean)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str, required=True,
                        help="yaml file for configuration")
    parser.add_argument('--checkpoint_path', type=str, default=True,
                        help="path of checkpoint pt file")
    parser.add_argument('-d', '--data_dir', type=str, default=True,
                        help="path of mixed, target files")

    args = parser.parse_args()

    hp = HParam(args.config)

    main(args, hp)
