import os
import sys
import time
import argparse
from concurrent import futures
import logging
import torch
#Grpc
import grpc
from dap_pb2_grpc import add_VfilterServicer_to_server
#Engine
from servicer import VoicefilterInference

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Voicefilter inference executor")
    parser.add_argument('-c', '--config',
                        nargs='?',
                        dest='config',
                        default='config/default.yaml',
                        help='Voicefilter config yaml file.',
                        type=str)
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model name.',
                        type=str)
    parser.add_argument('-r', '--dvectorize-remote',
                        nargs='?',
                        dest='dvectorize_remote',
                        required=True,
                        help='dvectorize remote URL',
                        type=str)
    parser.add_argument('-l', '--log-level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=53001)
    parser.add_argument('-d', '--device',
                        nargs='?',
                        dest='device',
                        help='gpu device',
                        type=int,
                        default=0)

    args = parser.parse_args()

    voicefilter = VoicefilterInference(args)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1), )
    add_VfilterServicer_to_server(voicefilter, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('voicefilter starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
