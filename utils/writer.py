import numpy as np
from torch.utils.tensorboard import SummaryWriter

from .plotting import plot_spectrogram_to_numpy


class MyWriter(SummaryWriter):
    def __init__(self, hp, logdir):
        super(MyWriter, self).__init__(logdir)
        self.hp = hp

    def log_training(self, train_loss, step):
        self.add_scalar('train_loss', train_loss, step)

    def log_evaluation(self, val_loss, sdr, n_count, sdr1,
                       mixed_wav, target_wav, est_wav, est_wav_mixed,
                       mixed_spec, mixed_phase, target_spec, target_phase, est_spec, est_phase, est_mask_mag, est_mask_phase,
                       phase_e_t, phase_m_t,
                       step):
        self.add_scalar('validate/mean_val_loss', val_loss, step)
        self.add_scalar('validate/mean_val_sdr', sdr, step)
        self.add_scalar('validate/negative_sdr_count', n_count, step)
        self.add_scalar('SDR', sdr1, step)
        self.add_scalar('test_loss/magnitude_error_mse', ((est_spec - target_spec) ** 2).mean(), step)

        phase_e_t = phase_e_t[..., 0] + 1j * phase_e_t[..., 1]
        angle = np.abs(np.angle(phase_e_t, deg=True))
        target_spec_sum = np.sum(target_spec)
        weight = target_spec / target_spec_sum
        phasedist = np.sum(weight * angle)

        phase_m_t = phase_m_t[..., 0] + 1j * phase_m_t[..., 1]
        angle_mixed = np.abs(np.angle(phase_m_t, deg=True))
        phasedist_mixed = np.sum(weight * angle_mixed)

        self.add_scalar('test_loss/phase_improv', phasedist_mixed - phasedist, step)

        self.add_audio('mixed_wav', mixed_wav, step, self.hp.audio.sample_rate)
        self.add_audio('target_wav', target_wav, step, self.hp.audio.sample_rate)
        self.add_audio('estimated_wav', est_wav, step, self.hp.audio.sample_rate)
        self.add_audio('estimated_mag_mixedphase', est_wav_mixed, step, self.hp.audio.sample_rate)

        self.add_image('data/mixed_spectrogram',
                       plot_spectrogram_to_numpy(mixed_spec), step, dataformats='HWC')
        self.add_image('data/mixed_spectrogram_phase',
                       plot_spectrogram_to_numpy(mixed_phase), step, dataformats='HWC')
        self.add_image('data/target_spectrogram',
                       plot_spectrogram_to_numpy(target_spec), step, dataformats='HWC')
        self.add_image('data/target_spectrogram_phase',
                       plot_spectrogram_to_numpy(target_phase), step, dataformats='HWC')
        self.add_image('data/mixed_phase_error_abs',
                       plot_spectrogram_to_numpy(np.abs(mixed_phase - target_phase)), step, dataformats='HWC')
        self.add_image('result/estimated_spectrogram',
                       plot_spectrogram_to_numpy(est_spec), step, dataformats='HWC')
        self.add_image('result/estimated_mask_mag',
                       plot_spectrogram_to_numpy(est_mask_mag), step, dataformats='HWC')
        self.add_image('result/estimated_phase',
                       plot_spectrogram_to_numpy(est_phase), step, dataformats='HWC')
        self.add_image('result/estimated_mask_phase',
                       plot_spectrogram_to_numpy(est_mask_phase), step, dataformats='HWC')
        self.add_image('result/estimation_mag_error_sq',
                       plot_spectrogram_to_numpy(np.square(est_spec - target_spec)), step, dataformats='HWC')
        self.add_image('result/estimation_phase_error_abs',
                       plot_spectrogram_to_numpy(np.abs(est_phase - target_phase)), step, dataformats='HWC')
