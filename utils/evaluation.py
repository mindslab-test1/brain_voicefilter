import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from mir_eval.separation import bss_eval_sources


def validate(audio, model, testloader, writer, step, hp):
    model.eval()
    num = 0
    sdr_list = list()
    val_loss_list = list()
    with torch.no_grad():
        for batch in testloader:
            dvec, target_wav, mixed_wav, target_mag, target_phase, mixed_mag, mixed_phase = batch[0]

            target_wav = torch.from_numpy(target_wav)
            target_wav = target_wav.cuda()
            target_mag = target_mag.unsqueeze(0).cuda()
            target_phase = target_phase.unsqueeze(0).cuda()
            mixed_mag = mixed_mag.unsqueeze(0).cuda()
            mixed_phase = mixed_phase.unsqueeze(0).cuda()

            dvec = dvec.unsqueeze(0).cuda()
            
            mixed_phase_cos, mixed_phase_sin = torch.cos(mixed_phase), torch.sin(mixed_phase)

            mixed_r = mixed_mag * mixed_phase_cos
            mixed_i = mixed_mag * mixed_phase_sin
            mixed = torch.stack((mixed_r, mixed_i), dim=-1)

            mask_mag, mask_phase = model(mixed, dvec)
            mask_mag, mask_phase = mask_mag.detach(), mask_phase.detach()

            est_mag = mixed_mag * mask_mag

            mask_phase_cos, mask_phase_sin = mask_phase[...,0], mask_phase[...,1]

            est_phase_cos = mixed_phase_cos * mask_phase_cos - mixed_phase_sin * mask_phase_sin
            est_phase_sin = mixed_phase_sin * mask_phase_cos + mixed_phase_cos * mask_phase_sin
            est_phase = torch.stack((est_phase_cos, est_phase_sin), dim=-1)
            
            est_mag, est_phase = est_mag[0], est_phase[0]

            mixed_phase_o = torch.stack((mixed_phase_cos, mixed_phase_sin), dim=-1)[0]
            est_wav_mixed = audio.spec2wav(est_mag, mixed_phase_o)

            est_wav = audio.spec2wav(est_mag, est_phase)
            
            dot_product = torch.dot(target_wav, est_wav)
            s_norm_square = torch.pow(torch.norm(target_wav, dim=0), 2) + hp.train.epsilon
            s_target = (dot_product/s_norm_square) * target_wav 
            
            e_noise = est_wav - target_wav
            
            val_loss = - 10 * torch.log10(torch.pow(torch.norm(s_target, dim=0), 2) / (torch.pow(torch.norm(e_noise, dim=0), 2) + hp.train.epsilon) + hp.train.epsilon)

            target_wav = target_wav.cpu().detach().numpy()
            est_wav = est_wav.cpu().detach().numpy()
            sdr = bss_eval_sources(target_wav, est_wav, False)[0][0]

            val_loss_list.append(val_loss)
            sdr_list.append(sdr)

            # checking spectrogram and audio about first one
            if num == 0:
                mixed_wav1 = mixed_wav
                target_wav1 = target_wav
                est_wav1 = est_wav

                sdr1 = sdr

                mixed_phase_c, mixed_phase_s = torch.cos(mixed_phase)[0], torch.sin(mixed_phase)[0]
                target_phase_c, target_phase_s = torch.cos(target_phase)[0], torch.sin(target_phase)[0]
                est_phase_c, est_phase_s = est_phase[...,0], est_phase[...,1]

                phase_e_t = torch.stack((est_phase_c*target_phase_c+est_phase_s*target_phase_s, est_phase_s*target_phase_c-est_phase_c*target_phase_s), dim=-1)
                phase_m_t = torch.stack((mixed_phase_c*target_phase_c+mixed_phase_s*target_phase_s, mixed_phase_s*target_phase_c-mixed_phase_c*target_phase_s), dim=-1)
                phase_e_t = phase_e_t.cpu().detach().numpy()
                phase_m_t = phase_m_t.cpu().detach().numpy()

                mixed_mag1 = mixed_mag[0].cpu().detach().numpy()
                mixed_phase1 = mixed_phase[0].cpu().detach().numpy()

                target_mag1 = target_mag[0].cpu().detach().numpy()
                target_phase1 = target_phase[0].cpu().detach().numpy()

                est_wav_mixed1 = est_wav_mixed.cpu().detach().numpy()

                est_mag1 = est_mag.cpu().detach().numpy()
                est_phase = torch.view_as_complex(est_phase).angle()
                est_phase1 = est_phase.cpu().detach().numpy()

                mask_mag1 = mask_mag[0].cpu().detach().numpy()
                mask_phase = torch.view_as_complex(mask_phase).angle()
                mask_phase1 = mask_phase[0].cpu().detach().numpy()
            num += 1

        sdr_mean = sum(sdr_list) / len(sdr_list)
        val_loss_mean = sum(val_loss_list) / len(val_loss_list)
        n_count = 0
        for s in sdr_list:
            if s < 0:
                n_count += 1

        writer.log_evaluation(val_loss_mean, sdr_mean, n_count, sdr1,
                              mixed_wav1, target_wav1, est_wav1, est_wav_mixed1,
                              mixed_mag1.T, mixed_phase1.T, target_mag1.T, target_phase1.T, est_mag1.T, est_phase1.T, mask_mag1.T, mask_phase1.T,
                              np.transpose(phase_e_t, (1,0,2)), np.transpose(phase_m_t, (1,0,2)),
                              step)

    model.train()
    return sdr_mean
