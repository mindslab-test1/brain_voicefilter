# adapted from Keith Ito's tacotron implementation
# https://github.com/keithito/tacotron/blob/master/util/audio.py

import librosa
import numpy as np
import torch

class Audio():
    def __init__(self, hp):
        self.hp = hp
        self.mel_basis = librosa.filters.mel(sr=hp.audio.sample_rate,
                                             n_fft=hp.embedder.n_fft,
                                             n_mels=hp.embedder.num_mels)

    def get_mel(self, y):
        y = librosa.core.stft(y=y, n_fft=self.hp.embedder.n_fft,
                              hop_length=self.hp.audio.hop_length,
                              win_length=self.hp.audio.win_length,
                              window='hann')
        magnitudes = np.abs(y) ** 2
        mel = np.log10(np.dot(self.mel_basis, magnitudes) + 1e-6)
        return mel

    def wav2spec(self, y):
        D = self.stft(y)
        D = torch.view_as_complex(D)
        S, P = D.abs(), D.angle()
        S, P = torch.transpose(S, 0, 1), torch.transpose(P, 0, 1) # to make [time, freq]
        return S, P 

    def spec2wav(self, mag, phase):
        mag, phase = torch.transpose(mag, 0, 1), torch.transpose(phase, 0, 1)
        stft_x = mag * phase[...,0]
        stft_y = mag * phase[...,1]
        stft_matrix = torch.stack((stft_x, stft_y), dim=-1)
        return self.istft(stft_matrix)

    def stft(self, y):
        return torch.stft(y, n_fft=self.hp.audio.n_fft,
                        hop_length=self.hp.audio.hop_length,
                        win_length=self.hp.audio.win_length,
                        window=torch.hann_window(self.hp.audio.win_length))

    def istft(self, stft_matrix):
        if stft_matrix.type() == 'torch.cuda.FloatTensor':
            win = torch.hann_window(self.hp.audio.win_length).cuda()
        else:
            win = torch.hann_window(self.hp.audio.win_length)
        return torch.istft(stft_matrix, n_fft=self.hp.audio.n_fft,
                        hop_length=self.hp.audio.hop_length,
                        win_length=self.hp.audio.win_length,
                        window=win)

    def amp_to_db(self, x):
        return 20.0 * np.log10(np.maximum(1e-5, x))

    def db_to_amp(self, x):
        return torch.pow(10.0, x * 0.05)

    def normalize(self, S):
        return torch.clamp(S / -self.hp.audio.min_level_db, -1.0, 0.0) + 1.0

    def denormalize(self, S):
        return (torch.clamp(S, 0.0, 1.0) - 1.0) * -self.hp.audio.min_level_db
