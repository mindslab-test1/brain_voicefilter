import os
import math
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import traceback
from tqdm import tqdm

from .audio import Audio
from .evaluation import validate
from model.model import VoiceFilter
from datasets.dataloader import create_dataloader


def train(args, pt_dir, chkpt_path, testloader, writer, logger, hp, hp_str):
    audio = Audio(hp)
    model = VoiceFilter(hp).cuda()
    if hp.train.optimizer == 'adam':
        optimizer = torch.optim.Adam(model.parameters(),
                                     lr=hp.train.adam)
    else:
        raise Exception("%s optimizer not supported" % hp.train.optimizer)

    step = 0

    topk = []
    topk_len = 3

    if chkpt_path is not None:
        logger.info("Resuming from checkpoint: %s" % chkpt_path)
        checkpoint = torch.load(chkpt_path)
        model.load_state_dict(checkpoint['model'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        step = checkpoint['step']

        # will use new given hparams.
        if hp_str != checkpoint['hp_str']:
            logger.warning("New hparams is different from checkpoint.")
    else:
        logger.info("Starting new training run")

    try:
        while True:
            trainloader = create_dataloader(hp, args, train=True)
            model.train()
            pbar = tqdm(total=hp.train.checkpoint_interval)
            for dvec, target_wav, mixed_mag, mixed_phase in trainloader:
                if step%10==0:
                    pbar.update(10)
                target_wav = target_wav.cuda()
                dvec = dvec.cuda()
                mixed_mag = mixed_mag.cuda()
                mixed_phase = mixed_phase.cuda()

                mixed = torch.stack((mixed_mag * torch.cos(mixed_phase), mixed_mag * torch.sin(mixed_phase)), dim=-1).cuda()
                
                mask_mag, mask_phase = model(mixed, dvec)
                
                output_mag = mask_mag * mixed_mag

                mask_phase_cos, mask_phase_sin = mask_phase[...,0], mask_phase[...,1]

                output_phase_cos = torch.cos(mixed_phase) * mask_phase_cos - torch.sin(mixed_phase) * mask_phase_sin
                output_phase_sin = torch.sin(mixed_phase) * mask_phase_cos + torch.cos(mixed_phase) * mask_phase_sin
                output_phase = torch.stack((output_phase_cos, output_phase_sin), dim=-1)
               
                output_wav_list = list()
                for i in range(hp.train.batch_size):
                    output_mag_b = output_mag[i]
                    output_phase_b = output_phase[i]
                                                  
                    output_wav_b = audio.spec2wav(output_mag_b, output_phase_b)
                    output_wav_list.append(output_wav_b)
                output_wav = torch.stack(output_wav_list, dim=0)
                
                target_wav = target_wav.unsqueeze(1) # [B, 1, T]
                output_wav = output_wav.unsqueeze(2) # [B, T, 1]
                dot_product = torch.matmul(target_wav, output_wav) # [B, 1, 1]
                s_norm_square = torch.pow(torch.norm(target_wav, dim=2, keepdim=True), 2) + hp.train.epsilon # [B, 1, 1]
                s_target = torch.matmul(dot_product / s_norm_square, target_wav) # [B, 1, T]
                s_target = s_target.squeeze(1) # [B, T]
            
                output_wav = output_wav.transpose(1,2) # [B, 1, T]
                e_noise = output_wav - target_wav # [B, 1, T]
                e_noise = e_noise.squeeze(1) # [B, T]
                
                sd_sdr = 10 * torch.log10(torch.pow(torch.norm(s_target,dim=1), 2)/(torch.pow(torch.norm(e_noise,dim=1), 2)+hp.train.epsilon)+hp.train.epsilon) # [B]
                loss = -torch.mean(sd_sdr)

                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                step += 1
    
                loss = loss.item()
                if loss > 1e8 or math.isnan(loss):
                    logger.error("Loss exploded to %.02f at step %d!" % (loss, step))
                    raise Exception("Loss exploded")
                
                # write loss to tensorboard
                if step % hp.train.summary_interval == 0:
                    writer.log_training(loss, step)
            pbar.close()
            # 1. save checkpoint file to resume training
            # 2. evaluate and save sample to tensorboard
            #if step % hp.train.checkpoint_interval == 0:
            acc = validate(audio, model, testloader, writer, step, hp)
            if len(topk) < topk_len or topk[topk_len - 1][1] < acc:
                save_path = os.path.join(pt_dir, 'chkpt_%d.pt' % step)
                torch.save({
                    'model': model.state_dict(),
                    'optimizer': optimizer.state_dict(),
                    'step': step,
                    'hp_str': hp_str,
                }, save_path)
                topk.append([save_path, acc])
                topk = sorted(topk, key=lambda t: -t[1])
                if len(topk) > topk_len:
                    poped = topk.pop()
                    os.remove(poped[0])
                logger.info("Saved checkpoint to: %s" % save_path)
                logger.info(topk)
            else:
                logger.info("step: %s" % step)
                logger.info("mean sdr: %.6f" % acc)
    except Exception as e:
        logger.info("Exiting due to exception: %s" % e)
        traceback.print_exc()
