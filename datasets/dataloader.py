import os
import glob
import torch
import random
import librosa
import numpy as np
from torch.utils.data import Dataset, DataLoader

from utils.audio import Audio


def create_dataloader(hp, args, train):
    def train_collate_fn(batch):
        dvec_list = list()
        target_wav_list = list()
        mixed_mag_list = list()
        mixed_phase_list = list()

        for dvec, target_wav, mixed_mag, mixed_phase in batch:
            dvec_list.append(dvec)
            target_wav_list.append(target_wav)
            mixed_mag_list.append(mixed_mag)
            mixed_phase_list.append(mixed_phase)

        dvec_list = torch.stack(dvec_list, dim=0)
        target_wav_list = torch.stack(target_wav_list, dim=0)
        mixed_mag_list = torch.stack(mixed_mag_list, dim=0)
        mixed_phase_list = torch.stack(mixed_phase_list, dim=0)
        return dvec_list, target_wav_list, mixed_mag_list, mixed_phase_list

    def test_collate_fn(batch):
        return batch

    if train:
        return DataLoader(dataset=VFDataset(hp, args, True),
                          batch_size=hp.train.batch_size,
                          shuffle=True,
                          num_workers=hp.train.num_workers,
                          collate_fn=train_collate_fn,
                          pin_memory=True,
                          drop_last=True,
                          sampler=None)
    else:
        return DataLoader(dataset=VFDataset(hp, args, False),
                          collate_fn=test_collate_fn,
                          batch_size=1, shuffle=False, num_workers=hp.train.num_workers)


class VFDataset(Dataset):
    def __init__(self, hp, args, train):
        def find_all(file_format):
            return sorted(glob.glob(os.path.join(self.data_dir, file_format)))
        self.hp = hp
        self.args = args
        self.train = train
        self.data_dir = hp.data.train_dir if train else hp.data.test_dir

        if self.train:
            train_folders = [x for x in glob.glob(os.path.join(args.libri_dir, 'train-clean', '*'))
                              if os.path.isdir(x)]
            train_spk = [glob.glob(os.path.join(spk, '**', hp.form.input), recursive=True)
                            for spk in train_folders]
            self.train_spk = [x for x in train_spk if len(x) >= 2]
        else:
            self.dvec_list = find_all(hp.form.dvec)
            self.target_wav_list = find_all(hp.form.target.wav)
            self.mixed_wav_list = find_all(hp.form.mixed.wav)
            assert len(self.dvec_list) == len(self.target_wav_list) == len(self.mixed_wav_list), \
                "number of validation files must match"
            assert len(self.dvec_list) != 0, \
                "no validation file found"
        
        self.audio = Audio(hp)

    def __len__(self):
        if self.train:
            return self.hp.train.batch_size * self.hp.train.checkpoint_interval
        else:
            return len(self.dvec_list)

    def __getitem__(self, idx):
        def mix(hp, audio, s1_target, s2):
            srate = hp.audio.sample_rate

            w1, _ = librosa.load(s1_target, sr=srate)
            w2, _ = librosa.load(s2, sr=srate)
            assert len(w1.shape) == len(w2.shape) == 1, \
                'wav files must be mono, not stereo'

            w1, _ = librosa.effects.trim(w1, top_db=20)
            w2, _ = librosa.effects.trim(w2, top_db=20)

            # fit audio to `hp.data.audio_len` seconds.
            # if merged audio is shorter than `L`, discard it
            L = int(srate * hp.data.audio_len)
            if w1.shape[0] < L or w2.shape[0] < L:
                return np.nan, np.nan, np.nan
            w1, w2 = w1[:L], w2[:L]

            if hp.train.mix_by_random_ratio:
                ratio = np.random.rand() + 0.5
                w1 = ratio * w1
                w2 = (2-ratio) * w2
            mixed = w1 + w2

            norm = np.max(np.abs(mixed)) * 1.1
            w1, w2, mixed = w1 / norm, w2 / norm, mixed / norm
            
            w1 = torch.from_numpy(w1) 
            mixed = torch.from_numpy(mixed)

            # save magnitude spectrograms
            mixed_mag, mixed_phase = audio.wav2spec(mixed)

            return w1, mixed_mag, mixed_phase

        if  self.train:
            while True:
                spk1, spk2 = random.sample(self.train_spk, 2)
                s1_dvec, s1_target = random.sample(spk1, 2)
                s1_dvec = os.path.splitext(s1_dvec)[0] + '_dvec.pt'
                dvec = torch.load(s1_dvec)
                s2 = random.choice(spk2)
                target_wav, mixed_mag, mixed_phase = mix(self.hp, self.audio, s1_target, s2)
                if type(target_wav) != float:
                    break

            return dvec, target_wav, mixed_mag, mixed_phase
        else:
            with open(self.dvec_list[idx], 'r') as f:
                dvec_path = f.readline().strip()
            dvec_path = os.path.splitext(dvec_path)[0] + '_dvec.pt'
            dvec = torch.load(dvec_path)

            target_wav, _ = librosa.load(self.target_wav_list[idx], self.hp.audio.sample_rate)
            mixed_wav, _ = librosa.load(self.mixed_wav_list[idx], self.hp.audio.sample_rate)
            target_mag, target_phase = self.wav2magphase(self.target_wav_list[idx])
            mixed_mag, mixed_phase = self.wav2magphase(self.mixed_wav_list[idx])
            return dvec, target_wav, mixed_wav, target_mag, target_phase, mixed_mag, mixed_phase

    def wav2magphase(self, path):
        wav, _ = librosa.load(path, self.hp.audio.sample_rate)
        wav = torch.from_numpy(wav).float()
        mag, phase = self.audio.wav2spec(wav)
        return mag, phase
