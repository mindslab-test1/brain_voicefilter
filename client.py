import sys
import grpc
import io
import numpy as np
import argparse
import soundfile
from scipy.io.wavfile import read
import google.protobuf.empty_pb2 as empty

from dap_pb2_grpc import VfilterStub
from dap_pb2 import EmbList, WavBinary, MixedAndRef


class VfilterClient(object):
    def __init__(self, remote='127.0.0.1:53001', chunk_size=1048576):
        channel = grpc.insecure_channel(remote)
        self.stub = VfilterStub(channel)
        self.chunk_size = chunk_size

    def separate_target_from_wav(self, wav_binary, ref_binary):
        message = self._generate_wav_binary_iterator(wav_binary, ref_binary)
        #ref_binary = self._generate_wav_binary_iterator(ref_binary)
        return self.stub.SeparateTarget(message)

    def get_emb_config(self):
        return self.stub.GetEmbConfig(empty.Empty())

    def _generate_wav_binary_iterator(self, wav_binary, ref_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield MixedAndRef(mixed=wav_binary[idx:idx+self.chunk_size], ref=ref_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Voicefilter client')
    parser.add_argument('-r', '--remote', type=str, default='127.0.0.1:53001',
                        help='grpc: ip:port')
    parser.add_argument('-m', '--mixed', type=str, default='mixed.wav',
                        help='mixed file')
    parser.add_argument('-t', '--ref', type=str, default='ref.wav',
                        help='reference file of target voice')
    args = parser.parse_args()

    client = VfilterClient(remote=args.remote)

    dvec_config = client.get_emb_config()
    print(dvec_config)

    with open(args.mixed, 'rb') as rf1:
        wav = rf1.read()
    with open(args.ref, 'rb') as rf2:
        rf = rf2.read()
    filtered_result = client.separate_target_from_wav(wav, rf)

    wav_result = bytearray()
    for wav_binary in filtered_result:
        wav_result.extend(wav_binary.bin)
    wav_result = io.BytesIO(wav_result)
    sampling_rate, wav_result = read(wav_result)

    if len(wav_result.shape) == 2:
        wav_result = wav_result[:, 0]

    if wav_result.dtype == np.int16:
        wav_result = wav_result / 32768.0
    elif wav_result.dtype == np.int32:
        wav_result = wav_result / 2147483648.0
    elif wav_result.dtype == np.uint8:
        wav_result = (wav_result - 128) / 128.0

    wav_result = wav_result.astype(np.float32)

    soundfile.write('result.wav', wav_result, samplerate=16000)
